package com.example.candycrush;

import androidx.annotation.LongDef;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity  {

    int[] candies = {R.drawable.c_blue, R.drawable.c_green, R.drawable.c_orange, R.drawable.c_purple, R.drawable.c_red, R.drawable.c_yellow};
    int bigBall = R.drawable.stick;
    int megaBall = R.drawable.lollipop;
    int widthOfBlock, noOfBlock = 8, widthOfScreen, heightOfScreen;
    ArrayList<ImageView> candy = new ArrayList<>();
    int candyToBeDragged, candyToBeReplace;
    int notCandy = R.drawable.trasparent;
    Handler handler;
    int interval = 100;
    int minimum = 3;
    int k, i, mBall;
    int select, select1,tag;
    ImageView imageView;
    ArrayList<Integer> list = new ArrayList<>();
    ArrayList<Integer> diagonal = new ArrayList<>();
    String TAG = "<<MainActivity";
    Integer[] r1 = {0, 1, 2, 3, 4, 5, 6, 7};
    Integer[] r2 = {8, 9, 10, 11, 12, 13, 14, 15};
    Integer[] r3 = {16, 17, 18, 19, 20, 21, 22, 23};
    Integer[] r4 = {24, 25, 26, 27, 28, 29, 30, 31};
    Integer[] r5 = {32, 33, 34, 35, 36, 37, 38, 39};
    Integer[] r6 = {40, 41, 42, 43, 44, 45, 46, 47};
    Integer[] r7 = {48, 49, 50, 51, 52, 53, 54, 55};
    Integer[] r8 = {56, 57, 58, 59, 60, 61, 62, 63};

    Integer[] c1 = {0, 8, 16, 24, 32, 40, 48, 56};
    Integer[] c2 = {1, 9, 17, 25, 33, 41, 49, 57};
    Integer[] c3 = {2, 10, 18, 26, 34, 42, 50, 58};
    Integer[] c4 = {3, 11, 19, 27, 35, 43, 51, 59};
    Integer[] c5 = {4, 8, 16, 24, 32, 40, 48, 56};
    Integer[] c6 = {5, 8, 16, 24, 32, 40, 48, 56};
    Integer[] c7 = {6, 8, 16, 24, 32, 40, 48, 56};
    Integer[] c8 = {7, 8, 16, 24, 32, 40, 48, 56};

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        widthOfScreen = displayMetrics.widthPixels;
        heightOfScreen = displayMetrics.heightPixels;
        widthOfBlock = widthOfScreen / noOfBlock;
        createBoard();
        handler = new Handler();
        startRepeat();

        for (ImageView imageView : candy) {

            imageView.setOnTouchListener(new OnSwipeListener(this) {


                @Override
                void onSwipeLeft() {
                    super.onSwipeLeft();

                    candyToBeDragged = imageView.getId();
//                    Log.d(TAG, "onSwipeLeft: "+candyToBeDragged);
                    candyToBeReplace = candyToBeDragged - 1;
                    candyInterchange();

                }

                @Override
                void onSwipeRight() {
                    super.onSwipeRight();

                    candyToBeDragged = imageView.getId();
                    candyToBeReplace = candyToBeDragged + 1;
                    candyInterchange();
                }

                @Override
                void onSwipeTop() {
                    super.onSwipeTop();

                    candyToBeDragged = imageView.getId();
                    candyToBeReplace = candyToBeDragged - noOfBlock;
                    candyInterchange();
                }

                @Override
                void onTape() {
                    super.onTape();

                    if(imageView.getId() == select)
                    {
                        if((int) imageView.getTag() == tag )
                        {
                            checkColumnRow();
                        }
                    }
                    else if(imageView.getId() == select1)
                    {
                        checkDiagonal();
                    }
                }

                @Override
                void onSwipeBottom() {
                    super.onSwipeBottom();

                    candyToBeDragged = imageView.getId();
                    candyToBeReplace = candyToBeDragged + noOfBlock;
                    candyInterchange();
                }
            });
            }

    }

    private void checkRow() {

        for (i = 0; i <= 63; i++) {
            Integer[] notValid = {6, 7, 14, 15, 22, 23, 30, 31, 38, 39, 46, 47, 54, 55,63};
            List<Integer> list1 = Arrays.asList(notValid);
            list.clear();
            list.add((int) candy.get(i).getTag());
            if (!list1.contains(i)) {
                for (k = i + 1; k <= 63; k++) {
                    if ((int) candy.get(k).getTag() == (int) candy.get(i).getTag()) {
                        list.add((int) candy.get(i).getTag());
                        //         Log.d(TAG, "checkRow:=== " + (int) candy.get(i).getTag());
                    } else {
                        //          Log.d(TAG, "size======" + list.size());

                        if (list.size() >= minimum && list.size() <= 4) {
                            for (int l = i; l < (i + list.size()); l++) {
                                candy.get(l).setImageResource(notCandy);
                                candy.get(l).setTag(notCandy);
                                //           Log.d(TAG, "Remove :-> " + l);
                            }
                            list.clear();
                        } else if (list.size() >= 5) {
                            for (int j = i; j < (i + list.size()); j++) {

                                if (j == i) {
                                    candy.get(i).setImageResource(megaBall);
                                    candy.get(i).setTag(megaBall);
                                    select = candy.get(i).getId();
                                    tag= (int) candy.get(i).getTag();
                              //      Log.d(TAG, "checkRow: "+select);
                                } else {
                                    candy.get(j).setImageResource(notCandy);
                                    candy.get(j).setTag(notCandy);
                                }
                            }
                            list.clear();
                        }
                        break;
                    }
                }
            }

        }
        moveDownCandie();
    }

    private void checkColumn() {

        int i = 0;
        while (i < 64) {
            ///   Log.d(TAG, "i==="+i);
            list.clear();
            list.add((int) candy.get(i).getTag());
            //    Log.d(TAG, "checkColumn: " + (int) candy.get(i).getTag());
            int j = i + 8;
            while (j < 64) {
                if ((int) candy.get(j).getTag() == (int) candy.get(i).getTag()) {
                    list.add((int) candy.get(j).getTag());
                    //  Log.d(TAG, "checkRow:=== " + (int) candy.get(j).getTag());
                } else {
                    //    Log.d(TAG, "size======" + list.size());
                    break;
                }
                j = j + 8;
            }
            if (list.size() >= minimum && list.size() <=4) {
                k = i;
                //     Log.d(TAG, "Remove :-> j " + j + " k " + k);
                while (k < j) {
                    candy.get(k).setImageResource(notCandy);
                    candy.get(k).setTag(notCandy);

                    k = k + 8;
                }
                list.clear();
            } else if (list.size() >= 5) {
                int l = i;
                while (l < j) {

                    if (l == (j-8)) {
                        candy.get(l).setImageResource(bigBall);
                        candy.get(l).setTag(bigBall);
                        select1 = candy.get(l).getId();
                       Log.d(TAG, "checkColumn: "+select1);

                    } else {
                        candy.get(l).setImageResource(notCandy);
                        candy.get(l).setTag(notCandy);
                    }
                    l = l + 8;
                }
                list.clear();
            }
            i++;
        }

        moveDownCandie();
    }

    private void checkColumnRow() {

        int k = select / 8;
        List<Integer[]> rowList = Arrays.asList(r1, r2, r3, r4, r5, r6, r7, r8);
        List<Integer[]> columnList = Arrays.asList(c1, c2, c3, c4, c5, c6, c7, c8);
        //     Log.d(TAG, "k====="+k);
        Integer[] rowSelect = rowList.get(k);

        //    Log.d(TAG, "checkColumnRow: "+rowSelect.length);
        List<Integer> rList = Arrays.asList(rowSelect);

        int rFirst = rList.get(0);
        int rLast = rList.get(7);
        //   Log.d(TAG, "checkColumnRow: "+c);
    //    if (select == candyToBeDragged) {
            for (int i = rFirst; i <= rLast; i++) {

                if (rList.contains(i)) {
                    candy.get(i).setImageResource(notCandy);
                    candy.get(i).setTag(notCandy);
                    if (i == select) {
                        mBall = rList.indexOf(i);
                    }
                }
            }
            Integer[] columnSelect = columnList.get(mBall);
            List<Integer> cList = Arrays.asList(columnSelect);
            int cFirst = cList.get(0);
            int cLast = cList.get(7);
//             Log.d(TAG, "first===="+cList.get(0));
//             Log.d(TAG, "last==="+cList.get(7));
            for (int i = cFirst; i <cLast; i++) {

                if (cList.contains(i)) {
                    candy.get(i).setImageResource(notCandy);
                    candy.get(i).setTag(notCandy);
                }
            }
      //  }
        moveDownCandie();
    }

    private void checkDiagonal() {

        int c = select1;
        for (int j = 0; j <63 ; j++) {

            if(j==select1)
            {
                while (c<63)
                {

                    diagonal.add((int) candy.get(c).getTag());
                    c=c+9;
                    Log.d(TAG, "checkDiagonal: "+diagonal.size());
                }
                while (c>0)
                {
                    diagonal.add((int) candy.get(c).getTag());
                    c=c-9;
                    Log.d(TAG, "checkDiagonal: "+diagonal.size());

                }
                while (c<63)
                {
                    diagonal.add((int) candy.get(c).getTag());
                    c=c+7;
                    Log.d(TAG, "checkDiagonal: "+diagonal.size());
                }
                while (c>0)
                {
                    diagonal.add((int) candy.get(c).getTag());
                    c=c-7;
                    Log.d(TAG, "checkDiagonal: "+diagonal.size());
                }
            }

        }

        moveDownCandie();
    }

    private void moveDownCandie() {

        Integer[] firstRow = {0, 1, 2, 3, 4, 5, 6, 7};
        List<Integer> list = Arrays.asList(firstRow);
        for (int i = 55; i >= 0; i--) {

            if ((int) candy.get(i + noOfBlock).getTag() == notCandy) {

                candy.get(i + noOfBlock).setImageResource((int) candy.get(i).getTag());
                candy.get(i + noOfBlock).setTag(candy.get(i).getTag());
                candy.get(i).setImageResource(notCandy);
                candy.get(i).setTag(notCandy);

                if (list.contains(i) && (int) candy.get(i).getTag() == notCandy) {

                    int randomColor = (int) Math.floor(Math.random() * candies.length);
                    candy.get(i).setImageResource(candies[randomColor]);
                    candy.get(i).setTag(candies[randomColor]);
                }
            }
            for (int j = 0; j <8 ; j++) {

                if((int) candy.get(j).getTag() == notCandy)
                {

                    int randomColor = (int) Math.floor(Math.random() * candies.length);
                    candy.get(j).setImageResource(candies[randomColor]);
                    candy.get(j).setTag(candies[randomColor]);

                }
            }
        }

    }

    Runnable repeatChecker = new Runnable() {
        @Override
        public void run() {
            try {

                checkRow();
                checkColumn();
                moveDownCandie();
            } finally {

                handler.postDelayed(repeatChecker, interval);
            }
        }
    };

    void startRepeat() {

        repeatChecker.run();
    }

    private void candyInterchange() {

        int Background = (int) candy.get(candyToBeReplace).getTag();
        int Background1 = (int) candy.get(candyToBeDragged).getTag();
        candy.get(candyToBeDragged).setImageResource(Background);
        candy.get(candyToBeReplace).setImageResource(Background1);
        candy.get(candyToBeDragged).setTag(Background);
        candy.get(candyToBeReplace).setTag(Background1);
    }

    private void createBoard() {
        GridLayout gridLayout = findViewById(R.id.board);
        gridLayout.setRowCount(noOfBlock);
        gridLayout.setColumnCount(noOfBlock);
        gridLayout.getLayoutParams().width = widthOfScreen;
        gridLayout.getLayoutParams().height = widthOfScreen;

        for (int i = 0; i < noOfBlock * noOfBlock; i++) {

            imageView = new ImageView(this);
            imageView.setId(i);
            imageView.setLayoutParams(new ViewGroup.LayoutParams(widthOfBlock, widthOfBlock));
            imageView.setMaxHeight(widthOfBlock);
            imageView.setMaxWidth(widthOfBlock);
            int random = (int) Math.floor(Math.random() * candies.length);
            imageView.setImageResource(candies[random]);
            imageView.setTag(candies[random]);
            candy.add(imageView);
            gridLayout.addView(imageView);
        }
    }


}